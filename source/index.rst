================
Koha Manual (en)
================

Author: The Koha Community

.. include:: images.rst
.. toctree::
   :maxdepth: 2

   intro
   installation
   globalpreferences
   administration
   tools
   patrons
   cash_management
   circulation
   cataloging
   valuebuilder
   course_reserves
   serials
   acquisitions
   erm
   preservation
   lists
   reports
   opac
   searching
   plugins
   about
   ILL_requests
   implementation_checklist
   cron_jobs
   webservices
   apis_protocols
   extending_koha
   third_party_software
   hardware
   links
   faq
   license
